
# <tt>issuebot</tt>

Our friendly robot that reads issues and merge requests, and responds
inline.  It is designed around GitLab CI as the service that runs it,
and integrates tightly with it.


# modules

This is build around modules that can run separately, crash,
etc. without affecting the whole process.  Each module also returns
its results as a YAML dictionary to _stdout_ so that the core
_issuebot_ can format all the results into a nice report which is
posted in a single message.


## Variables

Things that need to be shared across multiple modules should be
exported as environment variables.  GitLab CI handles environment
variables quite well.  The _ApplicationID_ is also used to name the
APK and the local source folder, following F-Droid conventions.


* _ISSUEBOT_CURRENT_APPLICATION_ID_ - the _ApplicationID_ of the current app
* _ISSUEBOT_CURRENT_ISSUE_ID_ - the number of the GitLab Issue currently being processed
* _ISSUEBOT_CURRENT_SOURCE_URL_ - the URL to the source repo for the current app, if this is set, then a local copy of the source code will be available at _build/<ApplicationID>_


## API

There are lots of helper functions and methods for common operations
available to _issuebot_modules that subclass `IssuebotModule`.


## Module Setup

_issuebot_ will install and remove things for the modules.  It will
currently handle _apt-get_ and _pip_.  Add a space separated list of
packages to these variable names in a comment:

* _issuebot_apt_install_ - list of packages to install using `apt-get install`
* _issuebot_apt_remove_ - list of packages to uninstall using `apt-get purge`
* _issuebot_pip_install_ - list of packages to install with `pip install`
* _issuebot_pip_remove_ - list of packages to install with `pip uninstall`


## Return Result

```yaml
# a block of HTML compatible with GitLab Markdown
report: |
  <details>
  <summary>This stuff was found</summary>
  <ul>
  <li>scary</li>
  <li>beautiful</li>
  <li>awful</li>
  <li>ugly</li>
  </ul>
  </details>
emoji:
  - tada
labels:
  - on-fdroid.org
```

## `fdroid-bot` label

[_issuebot_](https://gitlab.com/fdroid/issuebot) runs on a schedule to
scan all new submissions and post its reports directly to the issue or
merge request.  Once _issuebot_ completes posting its report, it will
eadd the `fdroid-bot` label to the issue or merge request.  That tells
_issuebot_ to ignore the issue in future runs.  If you want _issuebot_
to run on any item again, remove the `fdroid-bot` label.  On the next
run, _issuebot_ will consider that issue as if it had not seen it.
