#!/usr/bin/env python3

import configparser
import glob
import inspect
import os
import re
import sys
import textwrap
import yaml
try:
    from yaml import CSafeLoader as SafeLoader
except ImportError:
    from yaml import SafeLoader

localmodule = os.path.realpath(
    os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), '..'))
if localmodule not in sys.path:
    sys.path.insert(0, localmodule)
from issuebot import IssuebotModule

CROWDIN_REGEX = re.compile(r'https?://crowdin.com/project/[a-zA-Z0-9_-]+')
TRANSIFEX_REGEX = re.compile(r'https?://www.transifex.com/projects/p/[a-zA-Z0-9_-]+')
WEBLATE_REGEX = re.compile(r'https://hosted.weblate.org/(engage|projects)/[a-zA-Z0-9_-]+')


class CheckForTranslations(IssuebotModule):

    def main(self):
        if not os.path.exists(self.metadata_file):
            return
        with open(self.metadata_file) as fp:
            data = yaml.load(fp.read(), Loader=SafeLoader)
        archive_policy = data.get('ArchivePolicy')
        if 'Disabled' in data \
           or (archive_policy and archive_policy.startswith('0 ')):
            return
        if 'Translation' in data:
            return

        transifex_file = os.path.join(self.source_dir, '.tx', 'config')
        if os.path.exists(transifex_file):
            print(self.metadata_file, 'should have Transifex link for Translation')
            self.reply['labels'].add('transifex')
            config = configparser.ConfigParser()
            config.read(transifex_file)
            for k in config.keys():
                if '.' in k:
                    self.reply['reportData']['url'] = ('https://www.transifex.com/projects/p/'
                                                       + k.split('.')[0])

        crowdin_file = os.path.join(self.source_dir, 'crowdin.yml')
        if os.path.exists(crowdin_file):
            print(crowdin_file, 'should have CrowdIn link for Translation')
            self.reply['labels'].add('crowdin')

        weblate_file = os.path.join(self.source_dir, '.weblate')
        if os.path.exists(weblate_file):
            print(weblate_file, 'should have Weblate link for Translation')
            self.reply['labels'].add('weblate')
            config = configparser.ConfigParser()
            config.read(weblate_file)
            self.reply['reportData']['url'] = ("{url}/{translation}"
                                               .format(**config['weblate']))

        if not self.reply['reportData'].get('url'):
            for f in (glob.glob(os.path.join(self.source_dir, 'README*'))
                      + glob.glob(os.path.join(self.source_dir, '*.md'))):
                with open(f, errors='surrogateescape') as fp:
                    for line in fp:
                        m = CROWDIN_REGEX.search(line)
                        if m:
                            self.reply['reportData']['url'] = m.group()
                            break
                        m = TRANSIFEX_REGEX.search(line)
                        if m:
                            self.reply['reportData']['url'] = m.group()
                            break
                        m = WEBLATE_REGEX.search(line)
                        if m:
                            self.reply['reportData']['url'] = m.group()
                            break

        if self.reply['labels']:
            self.reply['report'] = (
                self.get_source_scanning_header('Missing Translation Link')
                + textwrap.dedent("""\n
                ```yaml
                Translation: {url}
                ```
                """.format(url=self.reply['reportData']
                           .get('url', 'https://example.com/path/to/your/project'))
                )
            )
        if self.reply['labels']:
            self.reply['labels'].add('missing-translation')
        self.write_json()


if __name__ == "__main__":
    CheckForTranslations().main()
